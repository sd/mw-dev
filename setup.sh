composer update

# Remove if already present (such as when project is copied from another system)
rm -f ./LocalSettings.php

php maintenance/run.php install Wikipedia Wikiuser --pass=wikipassword \
  --server=http://localhost:8080 \
	--dbserver=database \
	--dbtype=mysql \
	--dbname=my_wiki \
	--dbprefix="" \
	--installdbuser=wikiuser \
	--installdbpass=wikipassword \
	--dbuser=wikiuser \
	--dbpass=wikipassword \
	--scriptpath="" \
	--extensions=TitleBlacklist,WikiEditor

# Overwrite installer-generated LocalSettings.php with our file from /var/www
cp ../LocalSettings.php ./LocalSettings.php

php maintenance/run.php update --quick

# Include every grant in bot password, too bad there isn't an --all-grants option
php maintenance/run.php createBotPassword --appid=bp --grants=basic,blockusers,createaccount,createeditmovepage,delete,editinterface,editmycssjs,editmyoptions,editmywatchlist,editpage,editprotected,editsiteconfig,highvolume,mergehistory,oversight,patrol,privateinfo,protect,rollback,sendemail,uploadeditmovefile,uploadfile,viewdeleted,viewmywatchlist,viewrestrictedlogs Wikiuser 12345678901234567890123456789012

# Create a spare account too
php maintenance/run.php createAndPromote Wikiuser2 wikipassword

# Add bot password
php maintenance/run.php createBotPassword --appid=bp --grants=basic,blockusers,createaccount,createeditmovepage,delete,editinterface,editmycssjs,editmyoptions,editmywatchlist,editpage,editprotected,editsiteconfig,highvolume,mergehistory,oversight,patrol,privateinfo,protect,rollback,sendemail,uploadeditmovefile,uploadfile,viewdeleted,viewmywatchlist,viewrestrictedlogs Wikiuser2 12345678901234567890123456789012
