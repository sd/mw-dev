FROM mediawiki

RUN rm -rf /var/www/html

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install unzip, required for processing composer packages
RUN  apt-get -y update \
    && apt-get install -y unzip \
    && rm -rf /var/lib/apt/lists/*

# Set up XDebug for PHP debugging
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && echo "[xdebug]\n\
xdebug.mode=develop,debug\n\
xdebug.client_host=host.docker.internal\n\
xdebug.start_with_request=yes\n" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# Set up phan
RUN pecl install ast \
    && echo "extension=ast.so" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

WORKDIR /var/www/html
